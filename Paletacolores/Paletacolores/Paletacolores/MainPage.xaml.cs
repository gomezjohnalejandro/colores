﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Paletacolores
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        void onSliderValueChanged(object sender, ValueChangedEventArgs args)
        {

            /* if (sender == id_sl_blue)
             {
                 DisplayAlert("Error", "Prueba de colores", "OK");
             }*/

            id_bv_background.BackgroundColor=Color.FromRgba((int)id_sl_red.Value,(int)id_sl_green.Value,(int)id_sl_blue.Value,(int)id_sl_alpha.Value);

            if (sender == id_sl_red) 
            {
                label_red.Text = Convert.ToString(id_sl_red.Value);
            }
            else if (sender == id_sl_green)
            {
                label_green.Text = Convert.ToString(id_sl_green.Value);
            }
             else if (sender == id_sl_blue)
            {
                label_blue.Text = id_sl_blue.Value.ToString();
            }
            else if (sender == id_sl_alpha)
            {
                label_alpha.Text = Convert.ToString(id_sl_alpha.Value);
            }

        }
        
	}
}
